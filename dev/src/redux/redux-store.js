import { combineReducers, legacy_createStore as createStore } from "redux";
import newsPageReducer from "./newsPageReducer";
import aboutUsPageReducer from "./aboutUsPageReducer";
import galleryPageReducer from "./galleryPageReducer";

let reducers = combineReducers({
    newsState: newsPageReducer,
    aboutPage: aboutUsPageReducer,
    galleryPage: galleryPageReducer,
});

let store = createStore(reducers);

export default store;