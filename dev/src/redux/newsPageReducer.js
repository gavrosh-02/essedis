const SET_POSTS = 'SET_POSTS';

let intialState = {
    newsPostData: [],
}

const newsPageReducer = (state = intialState, action) => {
    switch (action.type) {
        case SET_POSTS:
            return { 
                ...state,
                newsPostData: action.data,
            };
        
        default:
            return state;
    }
}

export const setPostsAC = data => ({ type: SET_POSTS, data });

export default newsPageReducer;