import React, {useState} from "react";
import './News.css';
import NewsSlider from "./NewsSlider";
import parse from 'html-react-parser';
import { TextEditorContainer } from "../../logicComponents/TextEditor/TextEditorContainer";
import Modal from "../../logicComponents/Modal/Modal";

const News = (props) => {

    props.newsPostData.sort((a, b) => b.id - a.id);

    const [modalActive, setModalActive] = useState(false);
    const [selectedId, setSelectedId] = useState(null);

    const handleDelete = () => {
        props.onHandleDelete(selectedId);
        setModalActive(false);
    }

    return (
        <div className="news-wrapper">
            <div className="container">
                {
                    props.newsPostData.map(post =>
                        <div key={post.id} className="newsElement">
                            <div className="info">
                                <div className="postOptions">
                                    <div className="date SegoeUI">
                                        {post.publicDate}
                                    </div>
                                    <div className="adminOptions">
                                        <span className="delete"><button onClick={() => {setModalActive(true); setSelectedId(post.id)}}>Удалить</button></span>
                                        <Modal active={modalActive} setModalActive={setModalActive}>
                                            <p>Вы действительно хотите удалить?</p>
                                            <br />
                                            <p>
                                                <span><button className="buttonPopUpYes" onClick={handleDelete}>Да</button></span>
                                                <span><button className="buttonPopUpNo" onClick={() => setModalActive(false)}>Нет</button></span>
                                            </p>
                                        </Modal>
                                        <span className="update"><button onClick={() => { props.onHandleUpdate(post.id) }}>Редактировать</button></span>
                                    </div>
                                </div>
          
                                <div className="bodyNews">
                                    {parse(post.newsInfo)}
                                </div>
                            </div>
                            <div className="slider">
                                <NewsSlider images={post.images} />
                            </div>
                        </div>
                    )
                }
                <TextEditorContainer 
                    onHandleUpdate={props.onHandleUpdate}
                    isShowUpdateForm={props.isShowUpdateForm}
                    idUpdate={props.idUpdate}
                />
            </div>
        </div>
    )
}

export default News;