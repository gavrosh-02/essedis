import axios from "axios";
import React, { useEffect } from "react";
import { setItemsDataAC, setQuantityAC } from "../../../redux/galleryPageReducer";
import { connect } from "react-redux";
import GalleryElement from "./GalleryElement";

const GalleryElementAPI = props => {

    useEffect(() => {
        axios.get('http://localhost/essedis/api/gallery/galleryItem.php/?id=' + props.galleryItemID)
            .then(response => {
                props.setItemsData(response.data.items[0].imagePaths);
                console.log(response.data.items[0].imagePaths);
            })
    }, [])

    return (
        <GalleryElement 
            galleryItemData={props.galleryItemData}
            galleryItemID={props.galleryItemID}

            setQuantity={props.setQuantity}
        />
    );
}


const mapStateToProps = state => {
    return {
        galleryItemData: state.galleryPage.galleryItemData,
        galleryItemID: state.galleryPage.galleryItemID
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setItemsData: data => {
            dispatch(setItemsDataAC(data));
        },
        setQuantity: number => {
            dispatch(setQuantityAC(number));
        }
    }
}

export const GalleryElementContainer = connect(mapStateToProps, mapDispatchToProps)(GalleryElementAPI);

