import React from "react";
import Slider from "react-slick";
import './GalleryElement.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const GalleryElement = props => {

    props.setQuantity(props.galleryItemData.length);

    let i = 0;
    const settings = {
        customPaging: function(i) {
            return (
              <a className="smallImages">
                <img src={`http://localhost/essedis/upload/gallery/galleryItems/${props.galleryItemID}/${props.galleryItemData[i++]}`} />
              </a>
            );
          },
        dots: true,
        infinite: true,
        fade: true,
        slidesToShow: 1,
        dotsClass: "slick-dots slick-thumb",
        infinite: true,
        speed: 500,
        slidesToScroll: 1,
        adaptiveHeight: true
    };

    return (
        <Slider {...settings}>
            {props.galleryItemData.map(slider => {
                return (<div key={i++} className="bigImages"><img src={`http://localhost/essedis/upload/gallery/galleryItems/${props.galleryItemID}/${slider}`} alt="234234" /></div>)
            })}
        </Slider>
    )
}

export default GalleryElement