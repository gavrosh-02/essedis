import { useEffect } from "react";
import { setGalleryItemIdAC, setItemsAC, setQuantityAC } from "../../redux/galleryPageReducer";
import Gallery from "./Gallery";
import { connect } from "react-redux";
import axios from "axios";


const GalleryAPI = props => {

    useEffect(() => {
        axios.get('http://localhost/essedis/api/gallery/gallery.php')
            .then(response => {
                props.setItems(response.data.items);
                console.log(response.data.items);
            })

    }, []);

    let onHandleDelete = id => {
        axios.get('http://localhost/essedis/api/gallery/delete-gallery-item.php/?id=' + id)
            .then(response => {
                console.log(response);
                if (response.status === 200) {
                    alert("Item has deleted!");
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    return (
        <Gallery 
            galleryData = {props.galleryData}
            setGalleryItemID={props.setGalleryItemID}
            itemsQuantity={props.itemsQuantity}

            onHandleDelete={onHandleDelete}
        />
    );
}


const mapStateToProps = state => {
    return {
        galleryData: state.galleryPage.galleryData,
        itemsQuantity: state.galleryPage.itemsQuantity
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setItems: data => {
            dispatch(setItemsAC(data));
        },
        setGalleryItemID: id => {
            dispatch(setGalleryItemIdAC(id));
        },
        setQuantity: number => {
            dispatch(setQuantityAC(number));
        }
    }
}

export const GalleryContainer = connect(mapStateToProps, mapDispatchToProps)(GalleryAPI);