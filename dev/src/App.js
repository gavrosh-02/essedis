import './App.css';
import Footer from './components/footer/Footer';
import ChooseHeader from './components/Header/variantsOfHeader/ChooseHeader';
import MainContent from './components/mainContent/MainContent';
import ScrollToTop from './logicComponents/ScrollToTop';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Contacts from './components/Contacts/Contacts';
import { NewsContainer } from './components/news/NewsContainer';
import { AboutUsContainer } from './components/aboutUs/AboutContainer';
import { GalleryContainer } from './components/Gallery/GalleryContainer';



const App = () => {

  return (
    <BrowserRouter>
      <ScrollToTop />

      <div className='app-wrapper'>

        <ChooseHeader />

        <Routes>
          <Route path='/' element={<MainContent />} />
          <Route path='/news' element={<NewsContainer />} />
          <Route path='/about' element={<AboutUsContainer/>}/>
          <Route path='/contacts' element={<Contacts/>}/>
          <Route path='/gallery' element={<GalleryContainer/>}/>
        </Routes>

        <div className='footerBackground'>
          <Footer />
        </div>

      </div>
    </BrowserRouter>
  );
}

export default App;
